package com.visionmate.test.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.figma.model.RPSType.PAPER;
import static com.figma.model.RPSType.ROCK;
import static com.figma.model.RPSType.SCISSOR;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Testing correctness of Rock Paper Scissor result logic")
class RPSResultLogic {

    @Test
    void paperBeatsRock() {
        assertTrue(PAPER.beats(ROCK));
        assertFalse(PAPER.beats(SCISSOR));
        assertFalse(PAPER.beats(PAPER));
    }

    @Test
    void rockBeatsScissor() {
        assertTrue(ROCK.beats(SCISSOR));
        assertFalse(ROCK.beats(PAPER));
        assertFalse(ROCK.beats(ROCK));
    }

    @Test
    void scissorBeatPaper() {
        assertTrue(SCISSOR.beats(PAPER));
        assertFalse(SCISSOR.beats(ROCK));
        assertFalse(SCISSOR.beats(SCISSOR));
    }

}