package com.figma.model;

public interface GameResult {
    String winner();
}
