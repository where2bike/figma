package com.figma.model;

import lombok.Getter;

import java.util.List;

import static com.figma.model.RockPaperScissors.PLAYER_ONE;
import static com.figma.model.RockPaperScissors.PLAYER_TWO;
import static com.figma.model.RockPaperScissors.TIE;

@Getter
public class PRSResult implements GameResult {

    private long tieCount;
    private long playerOneWins;
    private long playerTwoWins;
    private int numberOfRounds;
    private String winner;

    @Override
    public String winner() {
        return winner;
    }

    public PRSResult(List<RockPaperScissors.RPSSingleRound> results) {
        numberOfRounds = results.size();
        tieCount = results.stream().filter(result -> result.getWinner().equals(TIE)).count();
        playerOneWins = results.stream().filter(result -> result.getWinner().equals(PLAYER_ONE)).count();
        playerTwoWins = results.stream().filter(result -> result.getWinner().equals(PLAYER_TWO)).count();
        if (playerTwoWins == playerOneWins) {
            winner = TIE;
        } else {
            winner = playerOneWins > playerTwoWins ? PLAYER_ONE : PLAYER_TWO;
        }
    }
}
