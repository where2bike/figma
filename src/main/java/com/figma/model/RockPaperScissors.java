package com.figma.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class RockPaperScissors implements Game {

    private final Player readyPlayerOne;
    private final Player playerTwo;
    private final int numberOfRounds;
    private final List<RPSSingleRound> results;
    public static final String TIE = "TIE";
    public static final String PLAYER_ONE = "Player one";
    public static final String PLAYER_TWO = "Player two";

    public static class Builder {

        private Player readyPlayerOne;
        private Player playerTwo;
        private int numberOfRounds;

        public Builder setReadyPlayerOne(Player readyPlayerOne) {
            this.readyPlayerOne = readyPlayerOne;
            return this;
        }

        public Builder setPlayerTwo(Player playerTwo) {
            this.playerTwo = playerTwo;
            return this;
        }

        public Builder setNumberOfRounds(int numberOfRounds) {
            this.numberOfRounds = numberOfRounds;
            return this;
        }

        public RockPaperScissors build() {
            return new RockPaperScissors(readyPlayerOne, playerTwo, numberOfRounds);
        }
    }

    public RockPaperScissors(Player readyPlayerOne, Player playerTwo, int numberOfRounds) {
        this.readyPlayerOne = readyPlayerOne;
        this.playerTwo = playerTwo;
        this.numberOfRounds = numberOfRounds;
        this.results = new ArrayList<>(numberOfRounds);
    }

    @Override
    public PRSResult playGame() {
        IntStream.rangeClosed(1, numberOfRounds)
                .forEach(i -> results.add(new RPSSingleRound(readyPlayerOne.getUsersPick(), playerTwo.getUsersPick())));
        return new PRSResult(results);
    }

    @Getter
    public class RPSSingleRound {
        public RPSSingleRound(RPSType playerOnePick, RPSType playerTwoPick) {
            this.playerOnePick = playerOnePick;
            this.playerTwoPick = playerTwoPick;

            if (playerOnePick == playerTwoPick) {
                winner = TIE;
            } else if (playerOnePick.beats(playerTwoPick)) {
                winner = PLAYER_ONE;
            } else {
                winner = PLAYER_TWO;
            }
        }

        private final RPSType playerOnePick;
        private final RPSType playerTwoPick;
        private final String winner;
    }
}
