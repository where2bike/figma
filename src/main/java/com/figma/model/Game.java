package com.figma.model;

public interface Game {
    GameResult playGame();
}
