package com.figma.model;

import com.figma.service.GameServiceImpl;
import lombok.Builder;
import lombok.Getter;

import java.util.Optional;

@Getter
@Builder
public class Player {

    private String nick;
    private RPSType usersPick;

    public RPSType getUsersPick() {
        return Optional.ofNullable(usersPick).orElse(GameServiceImpl.randomEnum(RPSType.class));
    }

}
