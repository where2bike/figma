package com.figma.model;

public enum RPSType {
    ROCK {
        @Override
        public boolean beats(RPSType other) {
            return other == SCISSOR;
        }
    },
    PAPER {
        @Override
        public boolean beats(RPSType other) {
            return other == ROCK;
        }
    },
    SCISSOR {
        @Override
        public boolean beats(RPSType other) {
            return other == PAPER;
        }
    };

    public abstract boolean beats(RPSType other);

}
