package com.figma.service;

import com.figma.model.GameResult;
import com.figma.model.Player;
import com.figma.model.RPSType;
import com.figma.model.RockPaperScissors;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class GameServiceImpl implements GameService {

    private static final SecureRandom random = new SecureRandom();

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz) {
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

    @Override
    public GameResult playThePredefinedGame() {
        Player readyPlayerOne = Player.builder()
                .nick("F4t4lity")
                .usersPick(RPSType.PAPER)
                .build();

        Player playerTwo = Player.builder()
                .nick("Normal Norman")
                .build();

        RockPaperScissors rockPaperScissors = new RockPaperScissors.Builder()
                .setReadyPlayerOne(readyPlayerOne)
                .setPlayerTwo(playerTwo)
                .setNumberOfRounds(100)
                .build();

        return rockPaperScissors.playGame();
    }
}
