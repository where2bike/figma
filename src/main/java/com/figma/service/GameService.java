package com.figma.service;

import com.figma.model.GameResult;

public interface GameService {

    GameResult playThePredefinedGame();
}
