package com.figma.controller;


import com.figma.model.GameResult;
import com.figma.service.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/game")
public class GameController {

    private final GameService gameService;

    @RequestMapping(value = "/lets-play", method = RequestMethod.GET)
    public GameResult playGame() {
        GameResult result = gameService.playThePredefinedGame();
        return result;
    }

}
